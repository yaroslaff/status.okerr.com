# JS-powered Okerr Status Page

## Installation 
Just clone, put it on any hosting (including static and/or JAMStack hosting, such as [GitHub](https://pages.github.com/) and [GitLab](https://about.gitlab.com/stages-devops-lifecycle/pages/) Pages or [Netlify](https://netlify.com/)).
You should get working webpage displaying status of okerr project.

To configure it for your project, adjust value of `textid` in `config.json` file. 
Main status page for each project usually called `index`, but set `status` to other value if you need. Changing 
`server` is not required, but it can save one small HTTP request and make status page little faster.

To run it on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), clone this project and enable Pages for your site.

# Other okerr resources
- [Okerr main website](https://okerr.com/)
- [Okerr-server source code repository](gitlab.com/yaroslaff/okerr-dev/) and [okerr server wiki doc](https://gitlab.com/yaroslaff/okerr-dev/wikis/)
- [Okerr client (okerrupdate) repositoty](https://gitlab.com/yaroslaff/okerrupdate) and [okerrupdate wiki doc](https://gitlab.com/yaroslaff/okerrupdate/wikis/)
- [Okerrbench network server benchmark](https://gitlab.com/yaroslaff/okerrbench)
- [Okerr custom status page](https://gitlab.com/yaroslaff/okerr-status)
- [Okerr js-powered static status page](https://gitlab.com/yaroslaff/okerrstatusjs)
- [Okerr network sensor](https://gitlab.com/yaroslaff/sensor)
 
 
